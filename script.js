// Task-1

class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
}

class Baby extends Person {
  constructor(name, age, favoriteToy) {
    super(name, age);
    this.favoriteToy = favoriteToy;
  }

  play() {
    console.log(`Playing with ${this.favoriteToy}`);
  }
}

let Farruh = new Person("Farruh", 19);
let Nodir = new Baby("Nodir", 19, "Car");
console.log(Farruh);
console.log(Nodir);
Nodir.play();

// Task-2

class Person2 {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }

  describe() {
    console.log(`${this.name}, ${this.age} years old`);
  }
}

console.log(new Person2("Smith", 29).describe());
console.log(new Person2("Clara", 21).describe());
